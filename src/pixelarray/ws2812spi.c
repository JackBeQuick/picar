/*
 * ws2812spi.c
 *
 *  Created on: May 6, 2018
 *      Author: John Pomaro
 */

#include <sys/ioctl.h>
#include <bcm2835.h>
#include <errno.h>
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */
#include <stdio.h>    /* For printf() */
#include <stdlib.h>   /* For malloc, free */
#include <string.h>	  /* For memset */
#include <stdbool.h>
#include <limits.h>
#include "jpcolorlib/jpcolorlib.h"
#include "pixelarray.h"

/* My WS2812 Library defines */
#define NUM_PIXELS 			8
#define BYTES_PER_PIXEL 	3
#define BYTES_ENC_VS_ORG	4
#define COLOR_ORDER_RGB
//#define COLOR_ORDER_GRB

/* bcm2835 library status codes */
#define BCMLIB_SUCESS	1
#define BCMLIB_FAIL		0

/**
 * Lookup table for encoding 4 information bits into a 16 bit string which,
 * when shifted, will match the timing requirements of the ws2812b IC.
 */
uint16_t ws2812_nibble_LUT[] = {
	0x8888,	// 0
	0x888E, // 1
	0x88E8, // 2
	0x88EE, // 3
	0x8E88, // 4
	0x8E8E, // 5
	0x8EE8, // 6
	0x8EEE, // 7
	0xE888, // 8
	0xE88E, // 9
	0xE8E8, // A
	0xE8EE, // B
	0xEE88, // C
	0xEE8E, // D
	0xEEE8, // E
	0xEEEE  // F
};


/* TODO: Delete this.  Use Color struct presets instead. */
char scene_preset_01[] = {
//  G ,  R ,  B ,
     0, 128,   0,
	 0,  96,   0,
    12,   0,  12,
     0,   0,  36,
     0,   0,  36,
	12,   0,  12,
     0,  96,   0,
     0, 128,   0
};
char *pscene_preset_01 = scene_preset_01;

int ws2812spi_init(void) {
	int status = BCMLIB_FAIL;

	status = bcm2835_aux_spi_begin();
	if (status != BCMLIB_SUCESS) {
		fprintf(stderr, "bcm2835_init failed. Are you running as root??\n");
		return -1;
	}

	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	bcm2835_spi_setClockDivider(125/*BCM2835_SPI_CLOCK_DIVIDER_128*/);
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);

	return 1;
}

int ws2812spi_exit(void) {
	bcm2835_spi_end();
	return 1;
}

int ws2812spi_tx_byte_array(char* in, int num_pixels) {
	bcm2835_spi_writenb(in,  num_pixels * BYTES_PER_PIXEL * BYTES_ENC_VS_ORG);
	return 1;
}

/** Translate 1 byte into a 32-bit string which can be decoded by the WS2812
* @param color_value  8-bit value to be encoded
* @return 32-bit string formatted for WS2812 */
uint32_t ws2812spi_byte_encode(uint8_t byte_in) {
	return (((uint32_t)ws2812_nibble_LUT[(byte_in>>4) & 0x0F] << 16) &	// high nibble
			(ws2812_nibble_LUT[byte_in & 0x0F]));		// low nibble
}


/** Encode a 1-byte-per-color byte array into a 4-byte-per-color char array
 * that can be decoded by the WS2812.
 * @param *output_buffer  Output array.  Must be 4x larger than input length.
 * @param *input_buffer  Input array.  1-byte per color.
 * @param len  Number of bytes to encode.  Output will be 4 times longer.
 * @return Number of output bytes or -1 if failure. */
int ws2812spi_encode_char_n(char *output_buffer, char *input_buffer, int len) {
	/* Ensure no over index out of bounds. */;
	if(len > INT_MAX/4)
		return -1;

	unsigned int target_index = 0;
	unsigned int source_index;
	uint16_t temp;

	for(source_index = 0;  source_index < len; source_index++) {
		/* Encode upper nibble. */
		// byte_to_encode = input_buffer[source_index]
		// encoded_word = lut[byte_to_encode>>4];
		// output_buffer[i++] = (uint8_t)((encoded_word >> 8) & 0xFF);
		// output_buffer[i++] = (uint8_t)(encoded_word * 0xFF);


		temp = ws2812_nibble_LUT[
				(unsigned int)((input_buffer[source_index]>>4) & 0x0F)];
		output_buffer[target_index]   = (char)((temp >> 8) & 0xFF);
		output_buffer[target_index+1] = (char)(temp & 0xFF);
		target_index += 2;

		/* Encode lower nibble. */
		temp = ws2812_nibble_LUT[
				(unsigned int)(input_buffer[source_index] & 0x0F)];
		output_buffer[target_index]   = (char)((temp >> 8) & 0xFF);
		output_buffer[target_index+1] = (char)(temp & 0xFF);
		target_index += 2;

	//	output_buffer[target_index] =
	//			ws2812_nibble_LUT[input_buffer[source_index] & 0x0F];
	//	target_index += 2;
	}

	return target_index;
}

//
///** Encodes a single byte into a 4-byte pulse train to be understood by a
// * ws2812b IC.
// *
// * TODO: Test this.
// * @note: Don't use! Untested!
// */
//char* ws2812_encode_byte(char *byte_in) {
//	static char output[4];			// Output array.
//	uint16_t temp;			// Holds encoded signal to transfer to output buf.
//
//	/* Encode upper nibble. */
//	temp = ws2812_nibble_LUT[(((*byte_in)>>4) & 0x0F)];
//	/* Transfer to output buffer.  Force byte order. */
//	output[0] = (char)((temp >> 8) & 0xFF);
//	output[1] = (char)(temp & 0xFF);
//
//	/* Encode lower nibble. */
//	temp = ws2812_nibble_LUT[((*byte_in) & 0x0F)];
//	/* Transfer to output buffer.  Force byte order. */
//	output[2] = (char)((temp >> 8) & 0xFF);
//	output[3] = (char)(temp & 0xFF);
//}
//
///** Encodes a single byte into a 4-byte pulse train to be understood by a
// * ws2812b IC.
// *
// * TODO: Test this.
// * @note: Don't use! Untested!
// */
//int ws2812_encode_ColorRGB_24bit(char *train_out, ColorRGB_24bit *pixel_in) {
//#ifdef COLOR_ORDER_RGB
//	/* TODO: I think this needs to use the stringcopy function. */
//	train_out[0] = ws2812_encode_byte(pixel_in->r);
//	train_out[4] = ws2812_encode_byte(pixel_in->g);
//	train_out[8] = ws2812_encode_byte(pixel_in->b);
//#else #ifdef COLOR_ORDER_GRB
//	train_out[0] = ws2812_encode_byte(pixel_in->g);
//	train_out[4] = ws2812_encode_byte(pixel_in->r);
//	train_out[8] = ws2812_encode_byte(pixel_in->b);
//#endif
//
//}
//
///** Encode a 1-byte-per-color byte array into a 4-byte-per-color char array
// * that can be decoded by the WS2812.
// * @param *output_buffer  Output array.  Must be 4x larger than input length.
// * @param *input_buffer  Input array.  Struct array, 1-byte per color.
// * @param len  Number of bytes to encode.  Output will be 4 times longer.
// * @return Number of output bytes or -1 if failure. */
//int ws2812_encode_ColorRGB_24bit_n(char *output_buffer, ColorRGB_24bit *input_buffer, int num_pixels) {
//	/* Ensure no over index out of bounds. */;
//	if(num_pixels > INT_MAX/(4*sizeof(*input_buffer)))
//		return -1;
//
//	// out[x..x+2] = encode(in[i])..encode(in[i+2])
//	unsigned int i_targ = 0;
//	unsigned int i_pixel;
//
//	for(i_pixel = 0;  i_pixel < num_pixels; i_pixel++) {
//		ws2812_encode_ColorRGB_24bit(output_buffer[i_targ += 4],input_buffer+i_pixel);
//	}
//
//	return i_targ;
//}

/*
 * jpcolorlib.c
 *
 *  Created on: May 6, 2018
 *      Author: John Pomaro
 */


#include "jpcolorlib.h"

#include <stdio.h>
#include <math.h>


/** Converts RGB-typed color struct to HSV-typed color struct.
 * Sourced but modified from https://stackoverflow.com/a/6930407/8134537 2018-04-16
 * 
 * @param rgb_in RGB float structure to be converted.
 * @return ColorHSV_float representation of input color.
 */
ColorHSV_float RGB_to_HSV_float(ColorRGB_float rgb_in) {
    ColorHSV_float         out;
    float      min, max, delta;

    min = rgb_in.r < rgb_in.g ? rgb_in.r : rgb_in.g;
    min = min  < rgb_in.b ? min  : rgb_in.b;

    max = rgb_in.r > rgb_in.g ? rgb_in.r : rgb_in.g;
    max = max  > rgb_in.b ? max  : rgb_in.b;

    out.v = max;                                // v
    delta = max - min;
    if (delta < 0.00001)
    {
        out.s = 0;
        out.h = 0; // undefined, maybe nan?
        return out;
    }
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        out.s = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0
        // s = 0, h is undefined
        out.s = 0.0;
        out.h = NAN;                            // its now undefined
        return out;
    }
    if( rgb_in.r >= max )                           // > is bogus, just keeps compilor happy
        out.h = ( rgb_in.g - rgb_in.b ) / delta;        // between yellow & magenta
    else
    if( rgb_in.g >= max )
        out.h = 2.0 + ( rgb_in.b - rgb_in.r ) / delta;  // between cyan & yellow
    else
        out.h = 4.0 + ( rgb_in.r - rgb_in.g ) / delta;  // between magenta & cyan

    out.h *= 60.0;                              // degrees

    if( out.h < 0.0 )
        out.h += 360.0;

    return out;
}

/** Converts HSV-typed struct to RGB-typed struct.
 * Sourced but modified from https://stackoverflow.com/a/6930407/8134537 2018-04-16
 *
 * @param hsv_in HSV float structure to be converted.
 * @return ColorRGB_float representation of input color.
 */
ColorRGB_float HSV_to_RGB_float(ColorHSV_float hsv_in) {
    float      hh, p, q, t, ff;
    long        i;
    ColorRGB_float out;

    if(hsv_in.s <= 0.0) {       // < is bogus, just shuts up warnings
        out.r = hsv_in.v;
        out.g = hsv_in.v;
        out.b = hsv_in.v;
        return out;
    }
    hh = hsv_in.h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = hsv_in.v * (1.0 - hsv_in.s);
    q = hsv_in.v * (1.0 - (hsv_in.s * ff));
    t = hsv_in.v * (1.0 - (hsv_in.s * (1.0 - ff)));

    switch(i) {
    case 0:
        out.r = hsv_in.v;
        out.g = t;
        out.b = p;
        break;
    case 1:
        out.r = q;
        out.g = hsv_in.v;
        out.b = p;
        break;
    case 2:
        out.r = p;
        out.g = hsv_in.v;
        out.b = t;
        break;

    case 3:
        out.r = p;
        out.g = q;
        out.b = hsv_in.v;
        break;
    case 4:
        out.r = t;
        out.g = p;
        out.b = hsv_in.v;
        break;
    case 5:
    default:
        out.r = hsv_in.v;
        out.g = p;
        out.b = q;
        break;
    }
    return out;
}

/** Populates a RGB float struct to a 24bit RGB struct.
 * @param *out 24bit RGB struct to be populated.
 * @param *in RGB float struct to source from.
 */
void RGB_float_populate_24bit(ColorRGB_24bit *out, ColorRGB_float *in) {
    out->r = (uint8_t)((*in).r * 255);
    out->g = (uint8_t)((*in).g * 255);
    out->b = (uint8_t)((*in).b * 255);
}

/** Prints a formatted string
 * @param *color Pointer to the 24 bit RGB color struct you wish to print.
 * TODO: Change this to sprintf instead of printf.
 */
void ColorRGB_24bit_print(ColorRGB_24bit *color) {
	printf("R[%3d] G[%3d] B[%3d]\n", (*color).r, (*color).g, (*color).b);
}

ColorRGB_24bit RGB_24bit_from_HSL_float_components(float hue, float sat, float val) {
    ColorHSV_float hsvcolor = { .h = hue, .s = sat, .v = val};
    ColorRGB_24bit out;
//    hsvcolor.h = hue;
//    hsvcolor.s = sat;
//    hsvcolor.v = val;

    ColorRGB_float temp = HSV_to_RGB_float(hsvcolor);
    RGB_float_populate_24bit(&out, &temp);
    return out;
}

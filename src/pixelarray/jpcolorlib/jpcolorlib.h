/*
 * jpcolorlib.h
 *
 *  Created on: May 6, 2018
 *      Author: John Pomaro
 */

#ifndef JPCOLORLIB_H_
#define JPCOLORLIB_H_

//#include <ctype.h>
#include <stdint.h>

static const float HUE_RED      =   0.0;
static const float HUE_AMBER    =  30.0;
static const float HUE_ORANGE   =  45.0;
static const float HUE_YELLOW   =  60.0;
static const float HUE_GREEN    = 120.0;
static const float HUE_CYAN     = 180.0;
static const float HUE_BLUE     = 240.0;
static const float HUE_VIOLET   = 270.0;
static const float HUE_MAGENETA = 300.0;
static const float HUE_ROSE     = 330.0;

typedef struct  {
    float r;       // Red component, a fraction between 0 and 1
    float g;       // Green component, a fraction between 0 and 1
    float b;       // Blue component, a fraction between 0 and 1
} ColorRGB_float;

typedef struct  {
    float h;       // hue angle in degrees
    float s;       // saturation fraction between 0 and 1
    float v;       // value fraction between 0 and 1
} ColorHSV_float;

typedef struct  {
	uint8_t r;      // Red component
	uint8_t g;      // Green component
	uint8_t b;      // Blue component
} ColorRGB_24bit;

void ColorRGB_24bit_print(ColorRGB_24bit *color);

ColorHSV_float RGB_to_HSV_float(ColorRGB_float in);
ColorRGB_float HSV_to_RGB_float(ColorHSV_float in);

void RGB_float_populate_24bit(ColorRGB_24bit *out, ColorRGB_float *in);
ColorRGB_24bit RGB_24bit_from_HSL_float_components(float hue, float sat, float val);


#endif /* JPCOLORLIB_H_ */

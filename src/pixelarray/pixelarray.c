/*
 * pixelarray.c
 *
 *  Created on: May 6, 2018
 *      Author: John Pomaro
 */

#include <stdio.h>
#include <stdlib.h>
#include "jpcolorlib/jpcolorlib.h"
#include "pixelarray.h"

//PixelArray front_strip = {
//        .pixels = ColorHSV_float[NUM_PIXELS]pixels,
//        .length = (int)NUM_PIXELS,
//        .pixel_type = "ColorHSV_float"};

///** Constructor for abstracted PixelArray where the pixels are of type ColorHSV_float.
// * PixelArray holds an array of pixels of arbitrary type (RGB, HSV, etc),
// * (24 bit, float, etc.).
// * Allocates memory accordingly.
// *
// * @param length number of pixels in the string
// * @return pointer to PixelArray[] in memory
// */
//PixelArray* PixelArray_Construct_from_ColorHSV_float(int length) {
//
//    /* Allocate memory for the structure. */
//    PixelArray* struct_ptr = malloc(sizeof(PixelArray));
//
//    /* Allocate memory for the pixel array. */
//    ColorHSV_float* pixarry_ptr = calloc(length, sizeof(ColorHSV_float));
//
//    /* Set the type string. */
//    const char* typestr = "ColorHSV_float";
//
//    struct_ptr->pixels = pixarry_ptr;
//    struct_ptr->length = length;
//    struct_ptr->pixel_type = typestr;
//
//    return struct_ptr;
//}

///** Destructor for abstracted PixelArray.
// * Frees the memory of the components of referenced by the PixelArray struct
// * and then the frees the memory of the struct itself.
// *
// * @param PixelArray_ptr Pointer to the PixelArray to be destructed.
// * @note NOT FINISHED.  DO NOT USE.
// */
//int PixelArray_Destroy(PixelArray* PixelArray_ptr) {
//
//	free((PixelArray_ptr->(ColorHSV_float*)pixels);
//    free(PixelArray_ptr);
//    return 0;
//}

//int PixelArray_test() {
//    puts("PixelArray test...");
//    PixelArray* astrip = PixelArray_Construct_from_ColorHSV_float(4);
//    printf("PixelArray initialized at address: %p\n", &astrip);
//
//    //PixelArray_Destroy(astrip);
//    return 0;
//
//}

int PixelArray_ColorRGB_24bit_init(ColorRGB_24bit **pixelArray, int num_elements) {
	*pixelArray =
			(ColorRGB_24bit*)calloc(num_elements, sizeof(*pixelArray));

	if (*pixelArray == NULL) {
		fprintf(stderr, "calloc failed in ColorRGB_24bit_init.\n");
		return -1;
	}
	printf("calloc success.  Memory allocated at %p\n", pixelArray);
	return num_elements * sizeof(*pixelArray);
}

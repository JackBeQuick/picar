/*
 * pixelarray.h
 *
 *  Created on: May 6, 2018
 *      Author: John Pomaro
 */

#ifndef PIXELARRAY_H_
#define PIXELARRAY_H_

#include "jpcolorlib/jpcolorlib.h"

/*========== Structures =====================================================*/
typedef struct {
	void* pixels;
	int length;
	const char* pixel_type;		/* i.e. RGB24, RGBW32, HSLFloat, etc. */
} PixelArray;

/** Allocates and clears memory for initializes a ColorRGB_24bit struct array.
 * @param *PixelArray Pixel array to allocate and initialize.
 * @param num_elements Number of elements desired.
 * @return Number of bytes allocated.
 */
int PixelArray_ColorRGB_24bit_init(ColorRGB_24bit **pixelArray, int num_elements);

//PixelArray* PixelArray_Construct_from_ColorHSV_float(int length);
//int PixelArray_Destroy(PixelArray* PixelArray_ptr);

#endif /* PIXELARRAY_H_ */

/*
 * joystick.h
 *
 *  Created on: Apr 27, 2018
 *      Author: john
 */

#ifndef JOYSTICK_JOYSTICK_H_
#define JOYSTICK_JOYSTICK_H_

/* ==== INCLUDES ============================================================*/
#include <stdint.h>

/* ==== DEFINES =============================================================*/
#define JOYSTICK_SUCCESS		1
#define JOYSTICK_ERR_FILE_DNE				-1
#define JOYSTICK_ERR_STRUCT_NOT_REGISTERED	-2

#define JS_EVENT_BUTTON         0x01    /* button pressed/released */
#define JS_EVENT_AXIS           0x02    /* joystick moved */
#define JS_EVENT_INIT           0x80    /* initial state of device */

/* ==== PUBLIC MEMBERS ======================================================*/
struct js_event {
	uint32_t time;		/* event timestamp in milliseconds */
	int16_t  value;		/* value */
	uint8_t  type;		/* event type */
	uint8_t  number;	/* axis/button number */
};

/* ==== PUBLIC METHODS ======================================================*/
/* for multi-threading test */
void *joystick_echo(volatile void *keep_going);

/** Enables copying event data into interfacing structure.
 * @return JOYSTICK_ERR... if failed. JOYSTICK_SUCCESS if successful.
 */
int joystick_enable_listening(void);

/** Disables copying event data into interfacing structure.
 * @return JOYSTICK_ERR... if failed. JOYSTICK_SUCCESS if successful.
 */
int joystick_stop_listening(void);

int joystick_init(void);

/** Reads the joystick and populates the event. Blocking.
 * @return 1 if sucessful and new data is in event.  -1 if failed.
 */
int joystick_read_event(struct js_event *js_event_out);

void joystick_print_event(struct js_event *event);

#endif /* JOYSTICK_JOYSTICK_H_ */

/*
 * dualshock3.h
 *
 *  Created on: Apr 27, 2018
 *      Author: john
 */

#ifndef JOYSTICK_DUALSHOCK3_H_
#define JOYSTICK_DUALSHOCK3_H_

#include <stdbool.h>

/* Analog axes */
#define AXIS_L3_X	0x00
#define AXIS_L3_Y	0x01
#define AXIS_R3_X	0x03
#define AXIS_R3_Y	0x04
#define AXIS_L2		0x02	/**< Rests at Minimum value */
#define AXIS_R2		0x05	/**< Rests at Minimum value */

/* Analog stick buttons */
#define BTN_L3	0x0b	///< Button on left analog stick
#define BTN_R3	0x0c	///< Button on right analog stick

/* Triggers */
#define BTN_L1	0x04
#define BTN_L2	0x06
#define BTN_R1	0x05
#define BTN_R2	0x07

/* D-Pad */
#define BTN_DPAD_UP	0x0d
#define BTN_DPAD_DN	0x0e
#define BTN_DPAD_LF	0x0f
#define BTN_DPAD_RT	0x10

/* Shapes */
#define BTN_SHAPE_CROSS	0x00	///< 'Cross'/'X' button
#define BTN_SHAPE_CIRCL	0x01	///< 'Circle' button
#define BTN_SHAPE_TRNGL	0x02	///< 'Triangle' button
#define BTN_SHAPE_SQARE	0x03	///< 'Square' button

/* Control buttons */
#define BTN_CNTRL_SELCT	0x08	///< 'Select' button
#define BTN_CNTRL_START	0x09	///< 'Start' button
#define BTN_CNTRL_PSORB	0x0a	///< Playstation Orb button

/* Min/Max values */
#define AXIS_L3_X_MIN -32767
#define AXIS_L3_X_MAX  32767
#define AXIS_L3_Y_MIN -32767
#define AXIS_L3_Y_MAX  32767
#define AXIS_R3_X_MIN -32767
#define AXIS_R3_X_MAX  32767
#define AXIS_R3_Y_MIN -32767
#define AXIS_R3_Y_MAX  32767
#define AXIS_L2_MIN -32767
#define AXIS_L2_MAX  32767
#define AXIS_R2_MIN -32767
#define AXIS_R2_MAX  32767
#define BTN_PRESSED 1
#define BTN_RELESED 0

struct dualshock_state {
	bool btn_shape_cross;
	bool btn_shape_circl;
	bool btn_shape_trngl;
	bool btn_shape_sqare;
	bool btn_l1;
	bool btn_l2;
	bool btn_r1;
	bool btn_r2;
	bool btn_cntrl_selct;
	bool btn_cntrl_start;
	bool btn_cntrl_psorb;
	bool btn_l3;
	bool btn_r3;
	bool btn_dpad_up;
	bool btn_dpad_dn;
	bool btn_dpad_lf;
	bool btn_dpad_rt;
	float axis_l3_x;
	float axis_l3_y;
	float axis_l2;
	float axis_r3_x;
	float axis_r3_y;
	float axis_r2;
};


#endif /* JOYSTICK_DUALSHOCK3_H_ */

/*
 * joystick.c
 *
 *  Created on: Apr 27, 2018
 *      Author: John Pomaro
 * Referenced from: https://www.kernel.org/doc/html/v4.14/input/joydev/joystick-api.html
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include "joystick.h"
#include "dualshock3.h"

const char* JOYSTICK_FILE = "/dev/input/js0";
int js_fd;		///< Joystick file descriptor. */
struct js_event event;

/* IOCTL operation  */  /* function                     3rd arg  */
#define JSIOCGAXES      /* get number of axes           char     */
#define JSIOCGBUTTONS   /* get number of buttons        char     */
#define JSIOCGVERSION   /* get driver version           int      */
#define JSIOCGNAME(len) /* get identifier string        char     */
#define JSIOCSCORR      /* set correction values        &js_corr */
#define JSIOCGCORR      /* get correction values        &js_corr */

/* Correction/calibration stats */
#define JS_CORR_NONE            0x00    /* returns raw values */
#define JS_CORR_BROKEN          0x01    /* broken line */

struct js_corr {
        int32_t  coef[8];
        uint16_t prec;
        uint16_t type;
};

struct js_properties {
	char	jsiocgaxes;
	char	jsiocgbuttons;
	int		jsiocgversion;
	char	jsiocgname;
	struct js_corr* jsiosgcorr;
	struct js_corr* jsiocgcorr;
};

bool listening_enabled = false;

int init_joystick(int *file_descriptor) {
	int errnum;
	*file_descriptor = open(JOYSTICK_FILE, O_RDONLY);

	if (*file_descriptor < 0) {
		fprintf(stderr, "Joystick '%s' does not exist.\n", JOYSTICK_FILE);
		errnum = errno;
		fprintf(stderr, "Errno: %d\n", errnum);
		perror("Error opening joystick file");
		return -1;
	}
	return 1;
}

int joystick_init(void) {
	return init_joystick(&js_fd);
}

int joystick_read_event(struct js_event *js_event_out) {
	int readretval;
	int errval;

	readretval = read(js_fd, js_event_out, sizeof(*js_event_out));
	if (readretval > 0)
		return 1;
	else {
		// Store the errno because we'll use it more than once.
		errval = errno;
		fprintf(stderr, "Error reading joystick file: [Errno %d] %s.\n",
				errval, strerror(errval));
		return -1;
	}
}

void joystick_print_event(struct js_event *event) {
	printf("Signal: %2d \tType: %2d \tValue: %2d \tTimestamp: %10d\n",
					(*event).number,
					(*event).type,
					(*event).value,
					(*event).time);
}

void *joystick_echo(volatile void *keep_going)
{
	if(init_joystick(&js_fd) == -1)
		exit(1);

	do {
		while (read(js_fd, &event, sizeof(event)) > 0) {
			joystick_print_event(&event);
		}
		/* EAGAIN is returned when the queue is empty */
		if (errno != EAGAIN) {
			// this means the buffer is empty.
			usleep(50000);
		}
		/* do something interesting with processed events */
	} while ((bool)keep_going == true);

	close(js_fd);
	return (0);
}

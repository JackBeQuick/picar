/*
 * drive.c
 *
 *  Created on: Apr 27, 2018
 *      Author: john
 */

/** Compile this with: gcc -o drive drive.c -lwiringPi -ncurses */
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <math.h>

#include <bcm2835.h>
#include "drive.h"
//#include "drive_lowlev.h"


#define TB6612_PWMB_GPIO 12 		// wPi 26
#define TB6612_PWMB_PWMCHAN 0	//
#define TB6612_BIN2_GPIO 16		// wPi 27
#define TB6612_BIN1_GPIO 20		// wPi 28
#define TB6612_STBY_GPIO 22		// wPi 3
#define TB6612_AIN1_GPIO 19		// wPi 24
#define TB6612_AIN2_GPIO 26		// wPi 25
#define TB6612_PWMA_GPIO 13		// wPi 23
#define TB6612_PWMA_PWMCHAN 1	//
#define PWM_RESOLUTION 1024
#define PWM_DIVIDER (int)(19200000/PWM_RESOLUTION/1000)

enum MotorState_enum { MOTOR_UNINTIALIZED, MOTOR_FORWARD, MOTOR_REVERSE, MOTOR_BRAKE, MOTOR_FREERUN };

enum MotorState_enum Motor_Left_State;
enum MotorState_enum Motor_Rght_State;

/* Scales a normalized input to a given range. */
#define scale_m(input, scaled_min, scaled_max) \
				(input * (scaled_max - scaled_min) + scaled_min)
uint32_t gpio_register_value;

int drive_init(void) {
	if (!bcm2835_init())
		return -1;
	/* Setup outputs for motor direction outputs */
	bcm2835_gpio_fsel(TB6612_STBY_GPIO, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(TB6612_AIN1_GPIO, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(TB6612_AIN2_GPIO, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(TB6612_BIN1_GPIO, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(TB6612_BIN2_GPIO, BCM2835_GPIO_FSEL_OUTP);

	/* Enable pull downs.  (NOT NEEDED.  TEST BEFORE USING). */
	// bcm2835_gpio_set_pud(TB6612_STBY_GPIO, BCM2835_GPIO_PUD_DOWN);
	// bcm2835_gpio_set_pud(TB6612_AIN1_GPIO, BCM2835_GPIO_PUD_DOWN);
	// bcm2835_gpio_set_pud(TB6612_AIN2_GPIO, BCM2835_GPIO_PUD_DOWN);
	// bcm2835_gpio_set_pud(TB6612_BIN1_GPIO, BCM2835_GPIO_PUD_DOWN);
	// bcm2835_gpio_set_pud(TB6612_BIN2_GPIO, BCM2835_GPIO_PUD_DOWN);

	/* Setup outputs for PWM function */
	bcm2835_gpio_fsel(TB6612_PWMA_GPIO, BCM2835_GPIO_FSEL_ALT0);
	bcm2835_gpio_fsel(TB6612_PWMB_GPIO, BCM2835_GPIO_FSEL_ALT0);
	bcm2835_pwm_set_clock(16);			//makes 1kHz, maybe?
    bcm2835_pwm_set_mode(TB6612_PWMA_PWMCHAN, 1, 1);
    bcm2835_pwm_set_mode(TB6612_PWMB_PWMCHAN, 1, 1);
    bcm2835_pwm_set_range(TB6612_PWMA_PWMCHAN, (int)PWM_RESOLUTION /*range*/);
    bcm2835_pwm_set_range(TB6612_PWMB_PWMCHAN, (int)PWM_RESOLUTION /*range*/);
    return 1;
}

/** Brings TB6612 motor driver out of standby.  Must be done before any motion.
 */
void drive_motor_standby_disable() {
	bcm2835_gpio_write(TB6612_STBY_GPIO, HIGH);
}

/** Brings TB6612 motor driver into standby.
 * Stops any motion and puts motor outputs in high-impedance.
 */
void drive_motor_standby_enable() {
	bcm2835_gpio_write(TB6612_STBY_GPIO, LOW);
}

void drive_setSpeed_MotorA(float speed) {
	speed =
		(speed > 1.0) ? 1.0 :
		(speed < 0) ? 0 :
		speed;
	bcm2835_pwm_set_data(TB6612_PWMA_PWMCHAN, (int)(PWM_RESOLUTION*speed));
}

void drive_setSpeed_MotorB(float speed) {
	speed =
		(speed > 1.0) ? 1.0 :
		(speed < 0) ? 0 :
		speed;
	bcm2835_pwm_set_data(TB6612_PWMB_PWMCHAN, (int)(PWM_RESOLUTION*speed));
}

void drive_setThrottle_independent(float lh_throttle, float rh_throttle) {
	/* Scale the normalized vector to the pwm resolution */
	int lh_speed = scale_m(lh_throttle, (0), PWM_RESOLUTION);
	int rh_speed = scale_m(rh_throttle, (0), PWM_RESOLUTION);
	
	/* Determine motor direction command */
	if (lh_speed > 0){
		gpio_register_value |=  (uint32_t)(1 << TB6612_BIN1_GPIO);
		gpio_register_value &= ~(uint32_t)(1 << TB6612_BIN2_GPIO);
	} else if (lh_speed < 0) {
		gpio_register_value |=  (uint32_t)(1 << TB6612_BIN2_GPIO);
		gpio_register_value &= ~(uint32_t)(1 << TB6612_BIN1_GPIO);
	}
	if (rh_speed > 0){
		gpio_register_value |=  (uint32_t)(1 << TB6612_AIN1_GPIO);
		gpio_register_value &= ~(uint32_t)(1 << TB6612_AIN2_GPIO);
	} else if (rh_speed < 0) {
		gpio_register_value |=  (uint32_t)(1 << TB6612_AIN2_GPIO);
		gpio_register_value &= ~(uint32_t)(1 << TB6612_AIN1_GPIO);
	}

	/* Cap the values to their limits */
	lh_speed = (lh_speed < 0) ? lh_speed * -1: lh_speed;
	lh_speed =
			(lh_speed > PWM_RESOLUTION) ? PWM_RESOLUTION :
					(lh_speed < 0) ? 0 :
							lh_speed;
	rh_speed = (rh_speed < 0) ? rh_speed * -1: rh_speed;
	rh_speed =
				(rh_speed > PWM_RESOLUTION) ? PWM_RESOLUTION :
						(rh_speed < 0) ? 0 :
								rh_speed;

	/* update the outputs */
	bcm2835_gpio_write_mask(gpio_register_value,
			/* mask */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO)
	);
	bcm2835_pwm_set_data(TB6612_PWMB_PWMCHAN, lh_speed);
	bcm2835_pwm_set_data(TB6612_PWMA_PWMCHAN, rh_speed);

}

/** Drives the depending upon a throttle and steering.  Utilizes skid-steer method.
 */
void drive_Drive(float throttle, float steering) {
	float Motor_left;
	float Motor_right;
	Motor_left  = throttle * ((steering >= 0)? 1 : 1+steering);
	Motor_right = throttle * ((steering <= 0)? 1 : 1-steering);
	drive_setSpeed_MotorA(Motor_right);
	drive_setSpeed_MotorB(Motor_left);
}


/** Sets the car in the forward direction.
 */
void drive_goForward(void) {
	/* IN1  = HIGH
	 * IN2  = LOW
	 * STBY = HIGH
	 */
	bcm2835_gpio_write_mask(
			/* value */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |0<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |0<< TB6612_BIN2_GPIO),
			/* mask */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO)
			);
}

/** Sets the car in the reverse direction.
 */
void drive_goReverse(void) {
	/* IN1  = LOW
	 * IN2  = HIGH
	 * STBY = HIGH
	 */
	bcm2835_gpio_write_mask(
			/* value */
			(uint32_t)(0<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |0<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO),
			/* mask */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO)
			);
}

void drive_Brake(void) {
	bcm2835_gpio_write_mask(
			/* value */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO),
			/* mask */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO)
			);
}

void drive_Coast(void) {
	bcm2835_gpio_write_mask(
			/* value */
			(uint32_t)(0<< TB6612_AIN1_GPIO
					  |0<< TB6612_AIN2_GPIO
					  |0<< TB6612_BIN1_GPIO
					  |0<< TB6612_BIN2_GPIO),
			/* mask */
			(uint32_t)(1<< TB6612_AIN1_GPIO
					  |1<< TB6612_AIN2_GPIO
					  |1<< TB6612_BIN1_GPIO
					  |1<< TB6612_BIN2_GPIO)
			);
}


int drive_test(int argc, char* argv[]) {
	/* initialize bcm2835 library. */
	if (!bcm2835_init())
		return 1;
	drive_init();

	drive_motor_standby_disable();
	drive_setSpeed_MotorA(0.8);
	drive_setSpeed_MotorB(0.8);
	drive_goForward();
	puts("going forward");
	usleep(0.5*1000000);
	drive_Brake();
	drive_Coast();
	usleep(0.5*1000000);
	drive_goReverse();
	puts("going backward");
	usleep(0.5*1000000);
	drive_Brake();
	drive_Coast();
	usleep(0.5*1000000);
	puts("did it work?");

	float throttle = 1.0;
	float direction = 0.0;
	puts("enter throttle");
	scanf("%f", &throttle);
	puts("enter direction");
	scanf("%f", &direction);
	drive_Drive(throttle, direction);
	drive_goForward();
	usleep(1*1000000);
	drive_Drive(0.0, 0.0);

	drive_Coast();
	drive_Brake();
	drive_motor_standby_enable();


	/* close and release bcm2835 library. */
	bcm2835_close();

	return 0;
}

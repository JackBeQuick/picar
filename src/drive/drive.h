/*
 * drive.h
 *
 *  Created on: Apr 29, 2018
 *      Author: John Pomaro
 */

#ifndef DRIVE_DRIVE_H_
#define DRIVE_DRIVE_H_

int drive_init(void);
int drive_test(int argc, char* argv[]);
void drive_motor_standby_disable();
void drive_motor_standby_enable();
void drive_setThrottle_independent(float lh_throttle, float rh_throttle);

#endif /* DRIVE_DRIVE_H_ */
